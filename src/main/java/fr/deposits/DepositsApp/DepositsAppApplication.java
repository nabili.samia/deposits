package fr.deposits.DepositsApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepositsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepositsAppApplication.class, args);
	}

}
