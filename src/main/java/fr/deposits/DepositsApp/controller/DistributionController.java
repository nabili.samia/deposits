package fr.deposits.DepositsApp.controller;

import fr.deposits.DepositsApp.entity.Distribution;
import fr.deposits.DepositsApp.entity.DistributionType;
import fr.deposits.DepositsApp.service.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DistributionController {

    @Autowired
    private DistributionService distributionService;

    @GetMapping("/distribute")
    public boolean distribute(@RequestParam(value = "userId") Long userId,
                              @RequestParam(value = "companyId") Long companyId,
                              @RequestParam(value = "amount") Double amount,
                              @RequestParam(value = "type") DistributionType type) {

       return distributionService.distribute(userId, companyId, amount, type);
    }

    @GetMapping("/distributions")
    public List<Distribution> distributions() {
        return distributionService.getAllDistributions();
    }
}