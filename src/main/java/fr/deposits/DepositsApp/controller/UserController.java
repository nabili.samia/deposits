package fr.deposits.DepositsApp.controller;

import fr.deposits.DepositsApp.entity.User;
import fr.deposits.DepositsApp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/balance")
    public Double userBalance(@RequestParam(value = "id") Long id) {
        return userService.computeBalance(id);
    }

    @GetMapping("/user")
    public User user(@RequestParam(value = "id") Long id) {
        return userService.getUser(id);
    }

    @GetMapping("/users")
    public List<User> users() {
        return userService.getAllUser();
    }
}