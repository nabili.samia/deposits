package fr.deposits.DepositsApp.entity;

public enum DistributionType {

    GIFT, MEAL
}
