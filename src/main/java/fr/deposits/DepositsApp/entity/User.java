package fr.deposits.DepositsApp.entity;


import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import javax.persistence.*;

@Entity
@Table(name = "user_table")
@Data
@Jacksonized
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;

}
