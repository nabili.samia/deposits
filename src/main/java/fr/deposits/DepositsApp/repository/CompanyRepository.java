package fr.deposits.DepositsApp.repository;

import fr.deposits.DepositsApp.entity.Company;
import fr.deposits.DepositsApp.entity.Distribution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

}

