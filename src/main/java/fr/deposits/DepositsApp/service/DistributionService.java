package fr.deposits.DepositsApp.service;

import fr.deposits.DepositsApp.entity.Company;
import fr.deposits.DepositsApp.entity.Distribution;
import fr.deposits.DepositsApp.entity.DistributionType;
import fr.deposits.DepositsApp.repository.CompanyRepository;
import fr.deposits.DepositsApp.repository.DistributionRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DistributionService {

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public boolean distribute(Long userId, Long companyId, Double amount, DistributionType type) {
        Company company = companyRepository.getById(companyId);
        Double newBalance = company.getBalance() - amount;
        if (newBalance > 0) {
            company.setBalance(newBalance);
            companyRepository.save(company);
            LocalDate now = LocalDate.now();
            Distribution distribution = Distribution.builder()
                    .distributionType(type)
                    .amount(amount)
                    .userId(userId)
                    .companyId(companyId)
                    .creationDate(LocalDate.now())
                    .expiryDate(type == DistributionType.GIFT ?
                            now.plusYears(1) :
                            now.withYear(now.getYear() + 1).withMonth(3).withDayOfMonth(1).minusDays(1))
                    .build();
            distributionRepository.save(distribution);
            return true;
        }
        return false;
    }

    public List<Distribution> getAllDistributions() {
        return distributionRepository.findAll();
    }

}
