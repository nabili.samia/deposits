package fr.deposits.DepositsApp.service;

import fr.deposits.DepositsApp.entity.Distribution;
import fr.deposits.DepositsApp.entity.User;
import fr.deposits.DepositsApp.repository.DistributionRepository;
import fr.deposits.DepositsApp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private UserRepository userRepository;

    public Double computeBalance(Long userId) {
        List<Distribution> distributionList = distributionRepository.findByUserId(userId);
        return distributionList.stream()
                .filter(d -> LocalDate.now().isBefore(d.getExpiryDate()))
                .mapToDouble(d -> d.getAmount())
                .sum();
    }

    public User getUser(Long userId) {
        return userRepository.findById(userId).get();
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }
}
