DROP TABLE user_table if EXISTS;
DROP TABLE company if EXISTS;
DROP TABLE distribution if EXISTS;

CREATE TABLE user_table (
id IDENTITY NOT NULL PRIMARY KEY,
name VARCHAR(255)
);

CREATE TABLE company (
id IDENTITY NOT NULL PRIMARY KEY,
name VARCHAR(255),
balance DECIMAL(18,2)
);

CREATE TABLE distribution (
id IDENTITY NOT NULL PRIMARY KEY,
distribution_type VARCHAR(255),
user_id BIGINT,
company_id BIGINT,
amount DECIMAL(18,2),
creation_date DATE,
expiry_date DATE
);


